CREATE TABLE  IF NOT EXISTS usr(
id BIGSERIAL PRIMARY KEY ,
name varchar(40) NOT NULL,
email varchar (255) NOT NULL
);

CREATE TABLE IF NOT EXISTS event(
id BIGSERIAL PRIMARY KEY,
title varchar(255) NOT NULL,
date date NOT NULL,
price integer NOT NULL DEFAULT 50
);

CREATE TABLE IF NOT EXISTS ticket(
id BIGSERIAL PRIMARY KEY ,
event_id bigint REFERENCES event (id),
user_id bigint REFERENCES usr(id),
category varchar(255) NOT NULL,
place integer NOT NULL
);

CREATE TABLE IF NOT EXISTS user_account(
id BIGSERIAL PRIMARY KEY,
user_id bigint REFERENCES usr(id) UNIQUE,
money integer NOT NULL DEFAULT 0
);