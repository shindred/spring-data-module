package com.learn.data.util.dateparser.impl;

import com.learn.data.controller.dateparser.impl.DateParserImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DateParserImplTest {

    @InjectMocks
    private DateParserImpl dateParser;

    @Test
    public void parseDateTest() throws ParseException {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

        Date expectedParsedDate;
        Date parsedDate;

        String dateToParse = "2020-01-01";
        expectedParsedDate = dateFormat.parse(dateToParse);

        parsedDate = dateParser.parseDate(dateToParse);
        assertThat(parsedDate).isEqualTo(expectedParsedDate);
    }
}