package com.learn.data.service.impl;

import com.learn.data.entity.User;
import com.learn.data.exception.UserNotFoundException;
import com.learn.data.exception.thrower.IdNotNullExceptionThrower;
import com.learn.data.exception.thrower.PaginationParameterExceptionThrower;
import com.learn.data.repository.UserRepository;
import com.learn.data.service.UserAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepositoryMock;

    @Mock
    private UserAccountService userAccountServiceMock;

    @Mock
    private IdNotNullExceptionThrower idNotNullExceptionThrowerMock;

    @Mock
    private User userMock;

    @Mock
    private PaginationParameterExceptionThrower throwerMock;

    @Test
    public void getUserById() {
        when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(userMock));
        userService.getUserById(1L);
        verify(userRepositoryMock, times(1)).findById(1L);
    }

    @Test
    public void getUserByExistentEmailTest() {
        when(userRepositoryMock.findUserByEmail("e")).thenReturn(Optional.of(userMock));
        userService.getUserByEmail("e");
        verify(userRepositoryMock, times(1)).findUserByEmail("e");
    }

    @Test
    public void getUsersByNameTest() {
        userService.getUsersByName("n", 1, 1);
        verify(userRepositoryMock, times(1)).findUsersByName("n", PageRequest.of(0, 1));
    }

    @Test
    public void createUserTest() {
        userService.createUser(userMock);
        verify(userRepositoryMock, times(1)).save(userMock);
    }

    @Test
    public void updateUserTest() {
        when(userMock.getId()).thenReturn(1L);
        when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(userMock));

        userService.updateUser(userMock);

        verify(userRepositoryMock, times(1)).save(userMock);
    }

    @Test
    public void updateNonExistentUserTest() {
        assertThrows(
                UserNotFoundException.class,
                () -> userService.updateUser(userMock)
        );
    }

    @Test
    public void deleteUserTest() {
        userService.deleteUser(1L);
        verify(userRepositoryMock, times(1)).deleteById(1L);
    }
}