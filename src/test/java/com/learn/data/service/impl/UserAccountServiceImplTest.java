package com.learn.data.service.impl;

import com.learn.data.entity.User;
import com.learn.data.entity.UserAccount;
import com.learn.data.exception.NotEnoughMoneyException;
import com.learn.data.exception.thrower.IdNotNullExceptionThrower;
import com.learn.data.repository.UserAccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserAccountServiceImplTest {

    @InjectMocks
    private UserAccountServiceImpl userAccountService;

    @Mock
    private UserAccountRepository userAccountRepositoryMock;

    @Mock
    private IdNotNullExceptionThrower throwerMock;

    @Mock
    private User userMock;

    @Mock
    private UserAccount userAccountMock;

    @Test
    public void findByUserTest() {
        when(userAccountRepositoryMock.findByUser(userMock)).thenReturn(Optional.of(userAccountMock));
        userAccountService.findByUser(userMock);
        verify(userAccountRepositoryMock, times(1)).findByUser(userMock);
    }

    @Test
    public void createTest() {
        when(userAccountMock.getUser()).thenReturn(userMock);
        userAccountService.create(userAccountMock);
        verify(userAccountRepositoryMock, times(1)).save(userAccountMock);
    }

    @Test
    public void refillTest() {
        when(userAccountRepositoryMock.findByUser(userMock)).thenReturn(Optional.of(userAccountMock));
        userAccountService.refill(userMock, 1);
        verify(userAccountRepositoryMock, times(1)).save(userAccountMock);
    }

    @Test
    public void withdrawTest() {
        when(userAccountMock.getMoney()).thenReturn(1);
        when(userAccountRepositoryMock.findByUser(userMock)).thenReturn(Optional.of(userAccountMock));
        userAccountService.withdraw(userMock, 1);
        verify(userAccountRepositoryMock, times(1)).save(userAccountMock);
    }

    @Test
    public void withdrawWithoutMoneyTest() {
        when(userAccountMock.getMoney()).thenReturn(0);
        when(userAccountRepositoryMock.findByUser(userMock)).thenReturn(Optional.of(userAccountMock));
        assertThrows(
                NotEnoughMoneyException.class,
                () -> userAccountService.withdraw(userMock, 1)
        );
    }
}