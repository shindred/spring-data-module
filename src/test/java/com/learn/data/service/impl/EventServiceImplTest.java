package com.learn.data.service.impl;

import com.learn.data.entity.Event;
import com.learn.data.exception.EventNotFoundException;
import com.learn.data.exception.thrower.IdNotNullExceptionThrower;
import com.learn.data.exception.thrower.PaginationParameterExceptionThrower;
import com.learn.data.repository.EventRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EventServiceImplTest {

    @InjectMocks
    private EventServiceImpl eventService;

    @Mock
    private PaginationParameterExceptionThrower throwerMock;

    @Mock
    private IdNotNullExceptionThrower idNotNullExceptionThrowerMock;

    @Mock
    private EventRepository eventRepositoryMock;

    @Mock
    private Event eventMock;

    @Test
    public void getEventByIdTest() {
        when(eventRepositoryMock.findById(1L)).thenReturn(Optional.of(eventMock));
        eventService.getEventById(1L);
        verify(eventRepositoryMock, times(1)).findById(1L);
    }

    @Test
    public void getEventByTitleTest() {
        eventService.getEventByTitle("", 1, 1);
        verify(eventRepositoryMock, times(1)).findEventsByTitle("", PageRequest.of(0, 1));
    }

    @Test
    public void getEventsForDayTest() {
        Date testDate = new Date();
        eventService.getEventsForDay(testDate, 1, 1);
        verify(eventRepositoryMock, times(1)).findEventsByDate(testDate, PageRequest.of(0, 1));
    }

    @Test
    public void createEventTest() {
        eventService.createEvent(eventMock);
        verify(eventRepositoryMock, times(1)).save(eventMock);
    }

    @Test
    public void updateEventTest() {
        when(eventMock.getId()).thenReturn(1L);
        when(eventRepositoryMock.findById(1L)).thenReturn(Optional.of(eventMock));
        eventService.updateEvent(eventMock);
        verify(eventRepositoryMock, times(1)).save(eventMock);
    }

    @Test
    public void updateEventWhenNotFoundTest() {
        assertThrows(
                EventNotFoundException.class,
                () -> eventService.updateEvent(eventMock)
        );
    }

    @Test
    public void deleteEventTest() {
        eventService.deleteEvent(1L);
        verify(eventRepositoryMock, times(1)).deleteById(1L);
    }
}