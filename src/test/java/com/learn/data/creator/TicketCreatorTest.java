package com.learn.data.creator;

import com.learn.data.entity.Event;
import com.learn.data.entity.Ticket;
import com.learn.data.entity.User;
import com.learn.data.entity.enumaration.TicketCategory;
import com.learn.data.service.EventService;
import com.learn.data.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TicketCreatorTest {

    @InjectMocks
    private TicketCreator ticketCreator;

    @Mock
    private UserService userServiceMock;

    @Mock
    private EventService eventServiceMock;

    @Mock
    private User userMock;

    @Mock
    private Event eventMock;

    @Test
    public void createTicketTest() {
        when(userServiceMock.getUserById(1L)).thenReturn(userMock);
        when(eventServiceMock.getEventById(1L)).thenReturn(eventMock);
        Ticket expectedTicket = new Ticket();
        expectedTicket.setEvent(eventMock);
        expectedTicket.setUser(userMock);
        expectedTicket.setTicketCategory(TicketCategory.STANDARD);
        expectedTicket.setPlace(1);
        Ticket actualTicket = ticketCreator.createTicket(1L, 1L, 1, TicketCategory.STANDARD);
        assertAll(
                () -> assertThat(actualTicket.getId()).isEqualTo(expectedTicket.getId()),
                () -> assertThat(actualTicket.getUser()).isEqualTo(expectedTicket.getUser()),
                () -> assertThat(actualTicket.getEvent()).isEqualTo(expectedTicket.getEvent()),
                () -> assertThat(actualTicket.getTicketCategory()).isEqualTo(expectedTicket.getTicketCategory()),
                () -> assertThat(actualTicket.getPlace()).isEqualTo(expectedTicket.getPlace())
        );
    }
}