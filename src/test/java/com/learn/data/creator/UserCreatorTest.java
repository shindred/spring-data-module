package com.learn.data.creator;

import com.learn.data.entity.User;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

public class UserCreatorTest {

    @Test
    public void createUserTest() {
        String name = "name";
        String email = "email";
        User expectedUser = new User();
        expectedUser.setName(name);
        expectedUser.setEmail(email);
        User actualUser = new UserCreator().createUser(name, email);
        assertAll(
                () -> assertThat(actualUser.getName()).isEqualTo(expectedUser.getName()),
                () -> assertThat(actualUser.getEmail()).isEqualTo(expectedUser.getEmail())
        );
    }
}