package com.learn.data.exception.thrower;

import com.learn.data.exception.PaginationParameterException;
import com.learn.data.exception.thrower.PaginationParameterExceptionThrower;
import org.junit.Test;

public class PaginationParameterExceptionThrowerTest {

    @Test(expected = PaginationParameterException.class)
    public void throwIfParametersAreLessThanOneTest() {
        PaginationParameterExceptionThrower thrower = new PaginationParameterExceptionThrower();
        thrower.throwIfParametersAreLessThanOne(-1, 1);
    }
}