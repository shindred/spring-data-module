package com.learn.data.exception.thrower;

import com.learn.data.exception.IdNotNullException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class IdNotNullExceptionThrowerTest {

    @Test
    public void throwIfIdNotNullTest() {
        assertThrows(
                IdNotNullException.class,
                () -> new IdNotNullExceptionThrower().throwIfIdNotNull(1L)
        );
    }
}