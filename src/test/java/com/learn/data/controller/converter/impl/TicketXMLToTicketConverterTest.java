package com.learn.data.controller.converter.impl;

import com.learn.data.entity.enumaration.TicketCategory;
import com.learn.data.entity.xml.TicketXML;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class TicketXMLToTicketConverterTest {

    @Test
    public void convertTest() {
        TicketXMLToTicketConverter converter = new TicketXMLToTicketConverter();
        TicketXML ticketXML = new TicketXML();
        ticketXML.setEventId(1L);
        ticketXML.setId(1L);
        ticketXML.setPlace(1);
        ticketXML.setEventId(1L);
        ticketXML.setTicketCategory(TicketCategory.STANDARD);
        assertNotNull(converter.convert(ticketXML));
    }
}