package com.learn.data.controller;

import com.learn.data.controller.dateparser.impl.DateParserImpl;
import com.learn.data.entity.Event;
import com.learn.data.service.EventService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@WebMvcTest(EventController.class)
public class EventControllerTest {

    @MockBean
    private EventService eventServiceMock;

    @MockBean
    private DateParserImpl dateParserMock;

    @MockBean
    private Event eventMock;

    @Autowired
    private MockMvc mockMvc;

    private final long id = 1L;
    private final int pageNum = 1;
    private final int pageSize = 1;

    @Test
    public void getActionsTest() throws Exception {
        this.mockMvc.perform(
                get("/event/actions")
        )
                .andExpect(status().isOk())
                .andExpect(view().name("event-actions"))
                .andExpect(content().string(containsString("Event actions")));
    }

    @Test
    public void getEventByIdWithoutPathVariableTest() throws Exception {
        this.mockMvc.perform(
                get("/event")
                        .param("id", String.valueOf(id))
        )
                .andExpect(redirectedUrl("/event/" + id))
                .andExpect(status().isFound());
    }

    @Test
    public void getEventByIdTest() throws Exception {
        when(eventServiceMock.getEventById(id)).thenReturn(eventMock);
        this.mockMvc.perform(
                get("/event/{id}", id)
        )
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("event"));
    }

    @Test
    public void getEventByTitleTest() throws Exception {
        when(eventServiceMock.getEventByTitle("", pageNum, pageSize)).thenReturn(Page.empty());

        this.mockMvc.perform(
                get("/event/by/title")
                        .param("title", "")
                        .param("pageNum", String.valueOf(pageNum))
                        .param("pageSize", String.valueOf(pageSize))
        )
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("eventsPage"));
    }

    @Test
    public void createEventTest() throws Exception {
        when(eventServiceMock.createEvent(any(Event.class))).thenReturn(eventMock);
        this.mockMvc.perform(
                post("/event")
                        .param("title", "")
                        .param("date", "2012-02-02")
                        .param("price", "1")
        )
                .andExpect(redirectedUrl("/event/" + eventMock.getId()))
                .andExpect(status().isFound());
        verify(eventServiceMock, times(1)).createEvent(any(Event.class));
    }

    @Test
    public void getEventsForDayTest() throws Exception {
        String testDateStr = "2012-02-02";
        Date dateMock = mock(Date.class);
        when(dateParserMock.parseDate(testDateStr)).thenReturn(dateMock);
        when(eventServiceMock.getEventsForDay(dateMock, pageNum, pageSize)).thenReturn(Page.empty());

        this.mockMvc.perform(
                get("/event/by/date")
                        .param("date", testDateStr)
                        .param("pageNum", String.valueOf(pageNum))
                        .param("pageSize", String.valueOf(pageSize))
        )
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("eventsPage"));
    }

    @Test
    public void updateEventTest() throws Exception {
        when(eventServiceMock.updateEvent(any(Event.class))).thenReturn(eventMock);
        this.mockMvc.perform(
                put("/event/{id}", id)
        )
                .andExpect(redirectedUrl("/event/" + id))
                .andExpect(status().isFound());
        verify(eventServiceMock, times(1)).updateEvent(any(Event.class));
    }

    @Test
    public void deleteEventByIdTest() throws Exception {
        doNothing().when(eventServiceMock).deleteEvent(id);
        this.mockMvc.perform(
                delete("/event/{id}", id)
        )
                .andExpect(redirectedUrl("/event/actions"))
                .andExpect(status().isFound());
        verify(eventServiceMock, times(1)).deleteEvent(id);
    }
}