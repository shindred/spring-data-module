package com.learn.data.controller;

import com.learn.data.controller.dateparser.DateParser;
import com.learn.data.controller.exporter.impl.TicketPDFExporter;
import com.learn.data.controller.importer.impl.TicketsImporter;
import com.learn.data.entity.Event;
import com.learn.data.entity.Ticket;
import com.learn.data.entity.User;
import com.learn.data.entity.enumaration.TicketCategory;
import com.learn.data.service.EventService;
import com.learn.data.service.TicketService;
import com.learn.data.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.data.domain.Page;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(TicketController.class)
@MockBeans({
        @MockBean(TicketPDFExporter.class),
        @MockBean(TicketsImporter.class),
        @MockBean(DateParser.class)
})
public class TicketControllerTest {

    @MockBean
    private TicketService ticketServiceMock;

    @MockBean
    private UserService userServiceMock;

    @MockBean
    private EventService eventServiceMock;

    @MockBean
    private Ticket ticketMock;

    @MockBean
    private User userMock;

    @MockBean
    private Event eventMock;

    @Autowired
    private MockMvc mockMvc;

    private final long userId = 1L;
    private final long eventId = 1L;
    private final TicketCategory category = TicketCategory.STANDARD;
    private final int pageNum = 1;
    private final int pageSize = 1;

    @Test
    public void getTicketActionsTest() throws Exception {
        this.mockMvc.perform(
                get("/ticket/actions")
        )
                .andExpect(status().isOk())
                .andExpect(view().name("ticket-actions"))
                .andExpect(content().string(containsString("Ticket actions")));
    }

    @Test
    public void bookTicketTest() throws Exception {
        int place = 1;
        when(ticketServiceMock.bookTicket(userId, eventId, place, category)).thenReturn(ticketMock);

        this.mockMvc.perform(
                post("/ticket")
                        .param("userId", String.valueOf(userId))
                        .param("eventId", String.valueOf(eventId))
                        .param("place", String.valueOf(place))
                        .param("ticketCategoryName", category.toString())
        )
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/ticket/actions"));
    }

    @Test
    public void getBookedTicketsByUserTest() throws Exception {
        when(userServiceMock.getUserById(userId)).thenReturn(userMock);
        when(ticketServiceMock.getBookedTicketsByUser(userMock, pageNum, pageSize)).thenReturn(Page.empty());

        this.mockMvc.perform(
                get("/ticket/by/user")
                        .param("userId", String.valueOf(userId))
                        .param("pageNum", String.valueOf(pageNum))
                        .param("pageSize", String.valueOf(pageSize))
        )
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("ticketsPage"));
    }

    @Test
    public void getBookedTicketsByEventTest() throws Exception {
        when(eventServiceMock.getEventById(eventId)).thenReturn(eventMock);
        when(ticketServiceMock.getBookedTicketsByEvent(eventMock, pageNum, pageSize)).thenReturn(Page.empty());

        this.mockMvc.perform(
                get("/ticket/by/event")
                        .param("eventId", String.valueOf(eventId))
                        .param("pageNum", String.valueOf(pageNum))
                        .param("pageSize", String.valueOf(pageSize))
        )
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("ticketsPage"));
    }

    @Test
    public void cancelTicketTest() throws Exception {
        long id = 1L;
        doNothing().when(ticketServiceMock).cancelTicket(id);

        this.mockMvc.perform(
                delete("/ticket/{id}", id)
        )
                .andExpect(redirectedUrl("/ticket/actions"))
                .andExpect(status().isFound());
        verify(ticketServiceMock, times(1)).cancelTicket(id);
    }

//    TODO test this
//    @Test
//    public void getPDFTicketsByUserTest() {
//    }

//    TODO test this
//    public void preloadTicketsFromXmlTest() {
//    }

//    TODO test this
//    @Test
//    public void uploadTicketsTest() {
//    }
}