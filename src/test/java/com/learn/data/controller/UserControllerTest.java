package com.learn.data.controller;

import com.learn.data.creator.UserCreator;
import com.learn.data.entity.User;
import com.learn.data.entity.UserAccount;
import com.learn.data.service.UserAccountService;
import com.learn.data.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @MockBean
    private UserService userServiceMock;

    @MockBean
    private UserAccountService userAccountServiceMock;

    @MockBean
    private UserCreator userCreatorMock;

    @MockBean
    private User userMock;

    @MockBean
    private UserAccount userAccountMock;

    @Autowired
    private MockMvc mockMvc;

    private final String name = "n";
    private final String email = "e";
    private final long userId = 1L;

    @Test
    public void getActionsTest() throws Exception {
        this.mockMvc.perform(
                get("/user/actions")
        )
                .andExpect(status().isOk())
                .andExpect(view().name("user-actions"))
                .andExpect(content().string(containsString("User actions")));
    }

    @Test
    public void getUserByIdWithoutPathVariableTest() throws Exception {
        this.mockMvc.perform(
                get("/user")
                        .param("id", String.valueOf(userId))
        )
                .andExpect(redirectedUrl("/user/" + userId))
                .andExpect(status().isFound());
    }

    @Test
    public void getUserByIdTest() throws Exception {
        when(userServiceMock.getUserById(userId)).thenReturn(userMock);
        when(userAccountServiceMock.findByUser(userMock)).thenReturn(userAccountMock);

        this.mockMvc.perform(
                get("/user/{id}", userId)
        )
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeExists("userAccount"));
    }

    @Test
    public void getUserByEmailTest() throws Exception {
        when(userServiceMock.getUserByEmail(email)).thenReturn(userMock);
        when(userAccountServiceMock.findByUser(userMock)).thenReturn(userAccountMock);

        this.mockMvc.perform(
                get("/user/by/email")
                        .param("email", email)
        )
                .andExpect(redirectedUrl("/user/" + userMock.getId()))
                .andExpect(status().isFound())
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeExists("userAccount"));
    }

    @Test
    public void getUsersByNamePerPageTest() throws Exception {
        int pageSize = 1;
        int pageNum = 1;
        when(userServiceMock.getUsersByName(name, pageNum, pageSize)).thenReturn(Page.empty());

        this.mockMvc.perform(
                get("/user/by/name")
                        .param("name", name)
                        .param("pageNum", String.valueOf(pageNum))
                        .param("pageSize", String.valueOf(pageSize))
        )
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("usersPage"));
    }

    @Test
    public void deleteUserByIdTest() throws Exception {
        this.mockMvc.perform(
                delete("/user/{id}", userId)
        )
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/user/actions"));
        verify(userServiceMock, times(1)).deleteUser(userId);
    }

    @Test
    public void updateUserByIdTest() throws Exception {
        this.mockMvc.perform(
                put("/user/{id}", userId)
        )
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/user/" + userId));
        verify(userServiceMock, times(1)).updateUser(any(User.class));
    }

    @Test
    public void createUserTest() throws Exception {
        when(userCreatorMock.createUser(name, email)).thenReturn(userMock);
        when(userServiceMock.createUser(userMock)).thenReturn(userMock);

        this.mockMvc.perform(
                post("/user")
                        .param("name", name)
                        .param("email", email)
        )
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/user/" + userMock.getId()));
        verify(userServiceMock, times(1)).createUser(userMock);
    }

    @Test
    public void refillTest() throws Exception {
        int amount = 1;
        when(userServiceMock.getUserById(userId)).thenReturn(userMock);

        this.mockMvc.perform(
                put("/user/refill/{userId}", userId)
                        .param("amount", String.valueOf(amount))
        )
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/user/" + userId));
        verify(userAccountServiceMock, times(1)).refill(userMock, amount);
    }
}