package com.learn.data.service;

import com.learn.data.entity.User;
import com.learn.data.entity.UserAccount;

public interface UserAccountService {

    UserAccount findByUser(User user);

    UserAccount create(UserAccount userAccount);

    void refill(User user, int amount);

    void withdraw(User user, int amount);
}
