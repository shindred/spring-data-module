package com.learn.data.service;

import com.learn.data.entity.User;
import com.learn.data.exception.IdNotNullException;
import com.learn.data.exception.PaginationParameterException;
import com.learn.data.exception.UserNotFoundException;
import org.springframework.data.domain.Page;

public interface UserService {

    /**
     * Gets user by its id.
     *
     * @return Created user
     * @throws UserNotFoundException if user wasn't found
     */
    User getUserById(long userId);

    /**
     * Gets user by it's email.
     *
     * @return User
     * @throws UserNotFoundException if user wasn't found
     */
    User getUserByEmail(String email);

    /**
     * Get list of users by matching name.
     *
     * @param name     Users name.
     * @param pageSize Pagination param. Number of users to return on a page.
     * @param pageNum  Pagination param. Number of the page to return. Starts from 1.
     * @return List of users
     * @throws UserNotFoundException        if users wasn't found
     * @throws PaginationParameterException if pageSize or pageNum  is less than zero
     */
    Page<User> getUsersByName(String name, int pageNum, int pageSize);

    /**
     * Creates new user.
     *
     * @param user User data.
     * @return Created user
     * @throws IdNotNullException If id is already predefined by user.
     */
    User createUser(User user);

    /**
     * Updates user using given data.
     *
     * @param user User data for update. Should have id set.
     * @return Updated User object.
     */
    User updateUser(User user);

    /**
     * Deletes user by its id.
     *
     * @param userId User id.
     */
    void deleteUser(long userId);
}
