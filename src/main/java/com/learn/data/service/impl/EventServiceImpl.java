package com.learn.data.service.impl;

import com.learn.data.entity.Event;
import com.learn.data.exception.EventNotFoundException;
import com.learn.data.exception.thrower.IdNotNullExceptionThrower;
import com.learn.data.exception.thrower.PaginationParameterExceptionThrower;
import com.learn.data.repository.EventRepository;
import com.learn.data.service.EventService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RequiredArgsConstructor
@Slf4j
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;

    private final PaginationParameterExceptionThrower paginationParameterExceptionThrower;

    private final IdNotNullExceptionThrower idNotNullExceptionThrower;

    @Override
    public Event getEventById(long eventId) {
        log.debug("Getting event with id: ".concat(String.valueOf(eventId)));
        return eventRepository.findById(eventId)
                .orElseThrow(EventNotFoundException::new);
    }

    @Override
    public Page<Event> getEventByTitle(String title, int pageNum, int pageSize) {
        paginationParameterExceptionThrower.throwIfParametersAreLessThanOne(pageNum, pageSize);
        log.debug("Getting events with title: ".concat(title));
        return eventRepository.findEventsByTitle(title, PageRequest.of(pageNum - 1, pageSize));
    }

    @Override
    public Page<Event> getEventsForDay(Date day, int pageNum, int pageSize) {
        paginationParameterExceptionThrower.throwIfParametersAreLessThanOne(pageNum, pageSize);
        log.debug("Getting events with date: ".concat(String.valueOf(day)));
        return eventRepository.findEventsByDate(day, PageRequest.of(pageNum - 1, pageSize));
    }

    @Override
    public Event createEvent(Event event) {
        idNotNullExceptionThrower.throwIfIdNotNull(event.getId());
        log.debug("Creating event: " + event.getTitle());
        return eventRepository.save(event);
    }

    @Override
    public Event updateEvent(Event event) {
        if (eventRepository.findById(event.getId()).isPresent()) {
            log.debug("Updating event with id: ".concat(String.valueOf(event.getId())));
            return eventRepository.save(event);
        } else {
            throw new EventNotFoundException();
        }
    }

    @Override
    public void deleteEvent(long eventId) {
        eventRepository.deleteById(eventId);
        log.debug("Deleted event with id: " + eventId);
    }
}