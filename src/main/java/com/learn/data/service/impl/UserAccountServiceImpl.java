package com.learn.data.service.impl;

import com.learn.data.entity.User;
import com.learn.data.entity.UserAccount;
import com.learn.data.exception.NotEnoughMoneyException;
import com.learn.data.exception.UserAccountNotFoundException;
import com.learn.data.exception.UserNotFoundException;
import com.learn.data.exception.thrower.IdNotNullExceptionThrower;
import com.learn.data.repository.UserAccountRepository;
import com.learn.data.service.UserAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserAccountServiceImpl implements UserAccountService {

    private final UserAccountRepository userAccountRepository;

    private final IdNotNullExceptionThrower idNotNullExceptionThrower;

    @Override
    public UserAccount findByUser(User user) {
        log.debug("Getting user account: " + user.getEmail());
        return userAccountRepository.findByUser(user)
                .orElseThrow(UserAccountNotFoundException::new);
    }

    @Override
    public UserAccount create(UserAccount userAccount) {
        idNotNullExceptionThrower.throwIfIdNotNull(userAccount.getId());
        log.debug("Creating user account for user: " + userAccount.getUser().getEmail());
        return userAccountRepository.save(userAccount);
    }

    @Override
    public void refill(User user, int amount) {
        userAccountRepository.findByUser(user)
                .ifPresent(uAcc -> {
                    int moneyAmount = uAcc.getMoney();
                    uAcc.setMoney(moneyAmount + amount);
                    log.debug("Refilled " + moneyAmount + "  for user: " + user.getEmail());
                    userAccountRepository.save(uAcc);
                });
    }

    @Override
    public void withdraw(User user, int amount) {
        userAccountRepository.findByUser(user)
                .ifPresent(uAcc -> {
                    int accountMoney = uAcc.getMoney();
                    if (accountMoney < amount){
                        throw new NotEnoughMoneyException();
                    }
                    uAcc.setMoney(accountMoney - amount);
                    log.debug("Withdrawn " + accountMoney + " from user :"  + user.getEmail());
                    userAccountRepository.save(uAcc);
                });
    }
}
