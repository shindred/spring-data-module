package com.learn.data.service.impl;

import com.learn.data.creator.TicketCreator;
import com.learn.data.entity.Event;
import com.learn.data.entity.Ticket;
import com.learn.data.entity.User;
import com.learn.data.entity.enumaration.TicketCategory;
import com.learn.data.exception.thrower.PaginationParameterExceptionThrower;
import com.learn.data.repository.TicketRepository;
import com.learn.data.service.EventService;
import com.learn.data.service.TicketService;
import com.learn.data.service.UserAccountService;
import com.learn.data.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;

    private final TicketCreator ticketCreator;

    private final UserService userService;

    private final EventService eventService;

    private final UserAccountService userAccountService;

    private final PaginationParameterExceptionThrower paginationParameterExceptionThrower;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = Exception.class)
    public Ticket bookTicket(long userId, long eventId, int place, TicketCategory ticketCategory) {
        Event event = eventService.getEventById(eventId);
        log.debug("Found event: " + event.getTitle());
        userAccountService.withdraw(userService.getUserById(userId), event.getPrice());
        log.debug("Withdrawing " + event.getPrice() + " from user with id: " + userId);
        return ticketRepository.save(ticketCreator.createTicket(userId, eventId, place, ticketCategory));
    }

    @Override
    public Page<Ticket> getBookedTicketsByUser(User user, int pageNum, int pageSize) {
        paginationParameterExceptionThrower.throwIfParametersAreLessThanOne(pageNum, pageSize);
        log.debug("Getting booked tickets for user with id: ".concat(String.valueOf(user.getId())));
        return ticketRepository.findBookedTicketsByUser(user, PageRequest.of(pageNum - 1, pageSize));
    }

    @Override
    public Page<Ticket> getBookedTicketsByEvent(Event event, int pageNum, int pageSize) {
        paginationParameterExceptionThrower.throwIfParametersAreLessThanOne(pageNum, pageSize);
        log.debug("Getting booked tickets for event with id: ".concat(String.valueOf(event.getId())));
        return ticketRepository.findBookedTicketsByEvent(event, PageRequest.of(pageNum - 1, pageSize));
    }

    @Override
    public void cancelTicket(long ticketId) {
        ticketRepository.deleteById(ticketId);
        log.debug("Deleted ticket with id: ".concat(String.valueOf(ticketId)));
    }

    @Override
    public List<Ticket> getAllTickets() {
        log.debug("Getting all tickets from DB");
        return ticketRepository.findAll();
    }

    @Override
    public List<Ticket> bookTickets(List<Ticket> tickets) {
        tickets.forEach(t -> {
            User user = t.getUser();
            int price = t.getEvent().getPrice();
            log.debug("Withdrawing " + price + " from user with id: " + user.getId());
            userAccountService.withdraw(user, price);
        });
        return ticketRepository.saveAll(tickets);
    }
}
