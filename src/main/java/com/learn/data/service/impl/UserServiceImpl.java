package com.learn.data.service.impl;

import com.learn.data.entity.UserAccount;
import com.learn.data.exception.thrower.IdNotNullExceptionThrower;
import com.learn.data.repository.UserRepository;
import com.learn.data.entity.User;
import com.learn.data.exception.UserNotFoundException;
import com.learn.data.service.UserAccountService;
import com.learn.data.service.UserService;
import com.learn.data.exception.thrower.PaginationParameterExceptionThrower;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PaginationParameterExceptionThrower paginationParameterExceptionThrower;

    private final IdNotNullExceptionThrower idNotNullExceptionThrower;

    private final UserAccountService userAccountService;

    @Override
    public User getUserById(long userId) {
        log.debug("Getting user with id: ".concat(String.valueOf(userId)));
        return userRepository.findById(userId)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User getUserByEmail(String email) {
        log.debug("Getting user with email: ".concat(String.valueOf(email)));
        return userRepository.findUserByEmail(email)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public Page<User> getUsersByName(String name, int pageNum, int pageSize) {
        paginationParameterExceptionThrower.throwIfParametersAreLessThanOne(pageNum, pageSize);
        log.debug("Getting users with name: ".concat(name));
        return userRepository.findUsersByName(name, PageRequest.of(pageNum - 1, pageSize));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public User createUser(User user) {
        idNotNullExceptionThrower.throwIfIdNotNull(user.getId());
        log.debug("Creating user: " + user.getName());
        User savedUser = userRepository.save(user);
        UserAccount userAccount = new UserAccount();
        userAccount.setUser(savedUser);
        userAccountService.create(userAccount);

        return savedUser;
    }

    @Override
    public User updateUser(User user) {
        if (userRepository.findById(user.getId()).isPresent()) {
            log.debug("Updating user with id: ".concat(String.valueOf(user.getId())));
            return userRepository.save(user);
        } else {
            throw new UserNotFoundException();
        }
    }

    @Override
    public void deleteUser(long userId) {
        userRepository.deleteById(userId);
        log.debug("User deleted by id: ".concat(String.valueOf(userId)));
    }
}