package com.learn.data.service;

import com.learn.data.entity.Event;
import com.learn.data.exception.EventNotFoundException;
import com.learn.data.exception.IdNotNullException;
import org.springframework.data.domain.Page;

import java.util.Date;

public interface EventService {

    /**
     * Gets event by its id.
     *
     * @param eventId Event id
     * @return Found event
     * @throws EventNotFoundException if event was not found.
     */
    Event getEventById(long eventId);

    /**
     * Get list of events by matching title.
     *
     * @param title    Event title.
     * @param pageSize Number of events to return on a page.
     * @param pageNum  Number of the page to return.
     * @return List of events or empty list if nothing was found.
     * @throws EventNotFoundException if events were not found.
     */
    Page<Event> getEventByTitle(String title, int pageNum, int pageSize);

    /**
     * Get list of events for specified day.
     *
     * @param day      Date object from which day information is extracted.
     * @param pageSize Pagination param. Number of events to return on a page.
     * @param pageNum  Pagination param. Number of the page to return. Starts from 1.
     * @return List of events or empty list if nothing was found.
     * @throws EventNotFoundException if events were not found.
     */
    Page<Event> getEventsForDay(Date day, int pageNum, int pageSize);

    /**
     * Creates new event.
     *
     * @param event Event data.
     * @return Created event
     * @throws IdNotNullException If id is already predefined by user.
     */
    Event createEvent(Event event);

    /**
     * Updates event using given data.
     *
     * @param event Event data for update.
     * @return Updated Event object
     */
    Event updateEvent(Event event);

    /**
     * Deletes event by its id.
     *
     * @param eventId Event id.
     */
    void deleteEvent(long eventId);
}
