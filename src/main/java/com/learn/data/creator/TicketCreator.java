package com.learn.data.creator;

import com.learn.data.entity.Ticket;
import com.learn.data.entity.enumaration.TicketCategory;
import com.learn.data.service.EventService;
import com.learn.data.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class TicketCreator {

    private final UserService userService;

    private final EventService eventService;

    public Ticket createTicket(long userId, long eventId, int place, TicketCategory ticketCategory) {
        log.debug("Initializing ticket");
        Ticket ticket = new Ticket();
        ticket.setUser(userService.getUserById(userId));
        ticket.setEvent(eventService.getEventById(eventId));
        ticket.setPlace(place);
        ticket.setTicketCategory(ticketCategory);
        log.debug("Ticket initialized");
        return ticket;
    }
}
