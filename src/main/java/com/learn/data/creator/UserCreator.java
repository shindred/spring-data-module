package com.learn.data.creator;

import com.learn.data.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UserCreator {

    public User createUser(String name, String email) {
        log.debug("Initializing user");
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        log.debug("User initialized");
        return user;
    }
}
