package com.learn.data.storage;

import com.learn.data.entity.Ticket;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "Tickets")
public class XmlTicketsStorage {

    private final List<Ticket> list;

    public XmlTicketsStorage() {
        list = new ArrayList<>();
    }

    public XmlTicketsStorage(List<Ticket> list) {
        this.list = list;
    }

    @XmlElement(name = "Ticket")
    public List<Ticket> getList() {
        return this.list;
    }
}