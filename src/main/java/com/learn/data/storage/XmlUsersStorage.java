package com.learn.data.storage;

import com.learn.data.entity.User;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "Users")
public class XmlUsersStorage {

    private final List<User> list;

    public XmlUsersStorage() {
        list = new ArrayList<>();
    }

    public XmlUsersStorage(List<User> list) {
        this.list = list;
    }

    @XmlElement(name = "User")
    public List<User> getList() {
        return this.list;
    }
}