package com.learn.data.controller.converter.impl;

import com.learn.data.entity.Ticket;
import com.learn.data.entity.xml.TicketXML;
import com.learn.data.controller.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TicketXMLToTicketConverter implements Converter<TicketXML, Ticket> {

    @Override
    public Ticket convert(TicketXML ticketXML) {
        Ticket ticket = new Ticket();
        ticket.setId(ticketXML.getId());
        ticket.setTicketCategory(ticketXML.getTicketCategory());
        ticket.setPlace(ticketXML.getPlace());
        return ticket;
    }
}
