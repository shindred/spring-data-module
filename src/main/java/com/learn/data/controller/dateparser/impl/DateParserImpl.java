package com.learn.data.controller.dateparser.impl;

import com.learn.data.controller.dateparser.DateParser;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@Slf4j
public class DateParserImpl implements DateParser {

    @Override
    public Date parseDate(String dateToParse) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat DateFor = new SimpleDateFormat(pattern);

        Date parsedDate;
        try {
            parsedDate = DateFor.parse(dateToParse);
        } catch (ParseException e) {
            parsedDate = new Date();
            log.warn("Can't parse date!");
        }

        return parsedDate;
    }
}
