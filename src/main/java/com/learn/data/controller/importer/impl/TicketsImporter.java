package com.learn.data.controller.importer.impl;

import com.learn.data.entity.Ticket;
import com.learn.data.storage.XmlTicketsStorage;
import com.learn.data.controller.importer.Importer;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;

import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

@Log4j2
@Component
@RequiredArgsConstructor
public class TicketsImporter implements Importer<List<Ticket>> {

    private final Jaxb2Marshaller marshaller;

    @Value("${path.resources}${path.xml.tickets}")
    private String path;

    @Override
    public List<Ticket> doImport() {
        File initialFile = new File(path);
        InputStream targetStream = null;
        try {
            targetStream = new FileInputStream(initialFile);
        } catch (FileNotFoundException e) {
            log.warn(e);
        }
        XmlTicketsStorage ticketsStorage = (XmlTicketsStorage) marshaller.unmarshal(new StreamSource(targetStream));

        return ticketsStorage.getList();
    }

    @Override
    public List<Ticket> doImportFromInputStream(InputStream inputStream) {
        XmlTicketsStorage ticketsStorage = (XmlTicketsStorage) marshaller.unmarshal(new StreamSource(inputStream));
        return ticketsStorage.getList();
    }
}