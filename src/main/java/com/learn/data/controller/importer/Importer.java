package com.learn.data.controller.importer;

import java.io.InputStream;

public interface Importer<O> {
    /**
     * Getting O object from predefined XML file
     *
     * @return O object
     */
    O doImport();

    /**
     * Getting O object from  InputStream
     *
     * @param inputStream InputStream
     * @return O object
     */
    O doImportFromInputStream(InputStream inputStream);
}