package com.learn.data.controller;

import com.learn.data.creator.UserCreator;
import com.learn.data.entity.User;
import com.learn.data.service.UserAccountService;
import com.learn.data.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("/user")
@Slf4j
public class UserController {

    private final UserService userService;

    private final UserAccountService userAccountService;

    private final UserCreator userCreator;

    @GetMapping("/actions")
    public String getActions() {
        log.info("Redirecting to an user-actions page");
        return "user-actions";
    }

    @GetMapping
    public String getUserById(@RequestParam long id) {
        String idStr = String.valueOf(id);
        log.info("Redirecting to an user page, id: ".concat(idStr));
        return "redirect:/user/".concat(idStr);
    }

    @GetMapping("/{id}")
    public String getUserById(Model model, @PathVariable long id) {
        log.info("Loading user from DB");
        User user = userService.getUserById(id);
        model.addAttribute("userAccount", userAccountService.findByUser(user));
        model.addAttribute("user", user);
        log.info("User loaded");
        return "user";
    }

    @GetMapping("/by/email")
    public String getUserByEmail(Model model, @RequestParam String email) {
        log.info("Loading user from DB");
        User foundUser = userService.getUserByEmail(email);
        model.addAttribute("userAccount", userAccountService.findByUser(foundUser));
        model.addAttribute("user", foundUser);
        log.info("User loaded");
        return "redirect:/user/".concat(String.valueOf(foundUser.getId()));
    }

    @GetMapping("/by/name")
    public String getUsersByNamePerPage(@RequestParam("name") String name,
                                        @RequestParam int pageNum,
                                        @RequestParam int pageSize,
                                        Model model) {
        log.info("Loading users from DB, name: ".concat(name));
        model.addAttribute("usersPage", userService.getUsersByName(name, pageNum, pageSize));
        log.info("Users loaded");
        return "users";
    }

    @DeleteMapping("/{id}")
    public String deleteUserById(@PathVariable long id) {
        userService.deleteUser(id);
        return "redirect:/user/actions";
    }

    @PutMapping("/{id}")
    public String updateUserById(User user, Model model) {
        model.addAttribute("user", userService.updateUser(user));
        return "redirect:/user/{id}";
    }

    @PostMapping
    public String createUser(@RequestParam String name, @RequestParam String email) {
        log.info("Saving user to a DB");
        User savedUser = userService.createUser(userCreator.createUser(name, email));
        log.info("User saved");
        return "redirect:/user/" + savedUser.getId();
    }

    @PutMapping("/refill/{userId}")
    public String refill(@PathVariable long userId, @RequestParam int amount) {
        log.info("Refilling user account for ".concat(String.valueOf(amount)));
        userAccountService.refill(userService.getUserById(userId), amount);
        log.info("User account refilled");
        return "redirect:/user/".concat(String.valueOf(userId));
    }
}
