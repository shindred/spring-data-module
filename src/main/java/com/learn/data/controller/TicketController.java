package com.learn.data.controller;

import com.learn.data.controller.dateparser.DateParser;
import com.learn.data.controller.exporter.impl.TicketPDFExporter;
import com.learn.data.controller.importer.impl.TicketsImporter;
import com.learn.data.entity.Event;
import com.learn.data.entity.Ticket;
import com.learn.data.entity.User;
import com.learn.data.entity.enumaration.TicketCategory;
import com.learn.data.service.EventService;
import com.learn.data.service.TicketService;
import com.learn.data.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/ticket")
@Slf4j
public class TicketController {

    private final TicketService ticketService;

    private final UserService userService;

    private final EventService eventService;

    private final TicketPDFExporter ticketPDFExporter;

    private final TicketsImporter ticketsImporter;

    private final DateParser dateParser;

    @GetMapping("/actions")
    public String getTicketActions() {
        log.info("Redirecting to a ticket-actions page");
        return "ticket-actions";
    }

    @PostMapping
    public String bookTicket(@RequestParam long userId,
                             @RequestParam long eventId,
                             @RequestParam int place,
                             @RequestParam String ticketCategoryName) {
        log.info("Saving ticket to DB");
        TicketCategory ticketCategory = TicketCategory.valueOf(ticketCategoryName.toUpperCase());
        Ticket savedTicket = ticketService.bookTicket(userId, eventId, place, ticketCategory);
        log.info("Saved ticket, id: ".concat(String.valueOf(savedTicket.getId())));
        return "redirect:/ticket/actions";
    }

    @GetMapping("/by/user")
    public String getBookedTicketsByUser(@RequestParam long userId,
                                         @RequestParam int pageNum,
                                         @RequestParam int pageSize,
                                         Model model) {
        log.info("Loading tickets from DB, user id: ".concat(String.valueOf(userId)));
        User user = userService.getUserById(userId);
        model.addAttribute("ticketsPage", ticketService.getBookedTicketsByUser(user, pageNum, pageSize));
        log.info("Tickets loaded");
        return "tickets";
    }

    @GetMapping("/by/event")
    public String getBookedTicketsByEvent(@RequestParam long eventId,
                                          @RequestParam int pageSize,
                                          @RequestParam int pageNum,
                                          Model model) {
        log.info("Loading tickets from DB, event id: ".concat(String.valueOf(eventId)));
        Event event = eventService.getEventById(eventId);
        model.addAttribute("ticketsPage", ticketService.getBookedTicketsByEvent(event, pageNum, pageSize));
        log.info("Tickets loaded");
        return "tickets";
    }

    @DeleteMapping("/{id}")
    public String cancelTicket(@PathVariable long id) {
        ticketService.cancelTicket(id);
        return "redirect:/ticket/actions";
    }

    @GetMapping(headers = "accept=application/pdf")
    public void getPDFTicketsByUser(HttpServletResponse response,
                                    @RequestParam long userId,
                                    @RequestParam int pageSize,
                                    @RequestParam int pageNum) throws IOException {
        Date currentDateTime = dateParser.parseDate(new Date().toString());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=tickets_".concat(currentDateTime.toString()).concat(".pdf");
        response.setHeader(headerKey, headerValue);
        log.info("Loading booked tickets from DB, user id: ".concat(String.valueOf(userId)));
        Page<Ticket> ticketsPage = ticketService.getBookedTicketsByUser(userService.getUserById(userId), pageSize, pageNum);
        log.info("Booked tickets loaded");
        log.info("Exporting found tickets");
        ticketPDFExporter.export(ticketsPage.getContent(), response.getOutputStream());
        log.info("Found tickets exported");
    }

    @PostMapping("/xml/import/all")
    public String preloadTicketsFromXml() {
        log.info("Preloading tickets from predefined xml");
        List<Ticket> tickets = ticketsImporter.doImport();
        ticketService.bookTickets(tickets);
        log.info("Ticket preloaded");
        return "redirect:/ticket/actions";
    }

    @PostMapping("/upload")
    public String uploadTickets(@RequestParam MultipartFile file) throws IOException {
        log.info("Importing tickets from file");
        ticketService.bookTickets(ticketsImporter.doImportFromInputStream(file.getInputStream()));
        log.info("Tickets imported from file");
        return "redirect:/ticket/actions";
    }
}
