package com.learn.data.controller;

import com.learn.data.entity.Event;
import com.learn.data.service.EventService;
import com.learn.data.controller.dateparser.impl.DateParserImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
@RequestMapping("/event")
@RequiredArgsConstructor
@Slf4j
public class EventController {

    private final EventService eventService;

    private final DateParserImpl dateParser;

    @GetMapping("/actions")
    public String getActions() {
        log.info("Redirecting to an event-actions page");
        return "event-actions";
    }

    @GetMapping
    public String getEventById(@RequestParam long id) {
        log.info("Redirecting to an event page, id: ".concat(String.valueOf(id)));
        return "redirect:/event/".concat(String.valueOf(id));
    }

    @GetMapping("/{id}")
    public String getEventById(Model model, @PathVariable long id) {
        log.info("Loading event page, id: " .concat(String.valueOf(id)));
        model.addAttribute("event", eventService.getEventById(id));
        return "event";
    }

    @GetMapping("/by/title")
    public String getEventByTitle(@RequestParam("title") String title,
                                  @RequestParam int pageNum,
                                  @RequestParam int pageSize,
                                  Model model) {
        log.info("Loading events from DB, title: ".concat(title));
        Page<Event> events = eventService.getEventByTitle(title, pageNum, pageSize);
        model.addAttribute("eventsPage", events);
        log.info("Events loaded");
        return "events";
    }

    @PostMapping
    public String createEvent(Event event, Model model) {
        log.info("Saving event");
        Event savedEvent = eventService.createEvent(event);
        Long savedEventId = savedEvent.getId();
        model.addAttribute("event", savedEvent);
        log.info("Event saved, id: ".concat(String.valueOf(savedEventId)));
        return "redirect:/event/" + savedEventId;
    }

    @GetMapping("/by/date")
    public String getEventsForDay(@RequestParam("date") String date,
                                  @RequestParam int pageNum,
                                  @RequestParam int pageSize,
                                  Model model) {
        Date parsedDate = dateParser.parseDate(date);
        log.info("Loading events from DB, date: ".concat(String.valueOf(parsedDate)));
        Page<Event> events = eventService.getEventsForDay(parsedDate, pageNum, pageSize);
        model.addAttribute("eventsPage", events);
        log.info("Events loaded");
        return "events";
    }

    @PutMapping("/{id}")
    public String updateEvent(Event event) {
        eventService.updateEvent(event);
        return "redirect:/event/{id}";
    }

    @DeleteMapping("/{id}")
    public String deleteEventById(@PathVariable long id) {
        eventService.deleteEvent(id);
        return "redirect:/event/actions";
    }
}
