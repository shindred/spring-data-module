package com.learn.data.repository;

import com.learn.data.entity.Event;
import com.learn.data.entity.Ticket;
import com.learn.data.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

    Page<Ticket> findBookedTicketsByEvent(Event event, Pageable pageable);

    Page<Ticket> findBookedTicketsByUser(User user, Pageable pageable);
}
