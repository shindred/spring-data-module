package com.learn.data.exception;

public class IdNotNullException extends IllegalArgumentException {
    public IdNotNullException() {
        super("Id must be null to create a new entity!");
    }
}
