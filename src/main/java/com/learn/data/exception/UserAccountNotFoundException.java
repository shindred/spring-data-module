package com.learn.data.exception;

import java.util.NoSuchElementException;

public class UserAccountNotFoundException extends NoSuchElementException {
    public UserAccountNotFoundException() {
        super("User account not found");
    }
}
