package com.learn.data.exception;

import java.util.NoSuchElementException;

public class TicketNotFoundException extends NoSuchElementException {
    public TicketNotFoundException() {
        super("Ticket not found");
    }
}
