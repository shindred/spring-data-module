package com.learn.data.exception.thrower;

import com.learn.data.exception.IdNotNullException;
import org.springframework.stereotype.Component;

@Component
public class IdNotNullExceptionThrower {

    public void throwIfIdNotNull(Long id) {
        if (id != null) {
            throw new IdNotNullException();
        }
    }
}
