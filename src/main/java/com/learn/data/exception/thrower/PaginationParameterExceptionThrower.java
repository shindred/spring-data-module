package com.learn.data.exception.thrower;

import com.learn.data.exception.PaginationParameterException;
import org.springframework.stereotype.Component;

@Component
public class PaginationParameterExceptionThrower {

    public void throwIfParametersAreLessThanOne(int firstParameter, int secondParameter) {
        if (firstParameter < 1 || secondParameter < 1) {
            throw new PaginationParameterException();
        }
    }
}
