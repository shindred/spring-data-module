package com.learn.data.exception;

public class PaginationParameterException extends IllegalArgumentException {
    public PaginationParameterException() {
        super("Page size or number cannot be less than one!");
    }
}
