package com.learn.data.exception;

import java.util.NoSuchElementException;

public class EventNotFoundException extends NoSuchElementException {
    public EventNotFoundException() {
        super("Event not found");
    }
}
