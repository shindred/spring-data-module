package com.learn.data.exception;

public class NotEnoughMoneyException extends IllegalArgumentException {
    public NotEnoughMoneyException() {
        super("Not enough money");
    }
}
