package com.learn.data.entity.xml;

import com.learn.data.entity.enumaration.TicketCategory;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class TicketXML {

    @XmlElement
    private long id;

    @XmlElement
    private long eventId;

    @XmlElement
    private long userId;

    @XmlElement
    private TicketCategory ticketCategory;

    @XmlElement
    private int place;
}
