package com.learn.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_account")
public class UserAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    @Getter
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    @Setter
    @Getter
    private User user;

    @Setter
    @Getter
    private int money;
}
