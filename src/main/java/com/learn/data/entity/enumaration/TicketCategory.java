package com.learn.data.entity.enumaration;

public enum TicketCategory {
    STANDARD, PREMIUM, BAR
}
