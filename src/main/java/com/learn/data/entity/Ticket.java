package com.learn.data.entity;

import com.learn.data.entity.enumaration.TicketCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Ticket implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    @Getter
    private Long id;

    @ManyToOne
    @JoinColumn(name = "event_id")
    @Setter
    @Getter
    private Event event;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @Setter
    @Getter
    private User user;

    @Setter
    @Getter
    @Enumerated(EnumType.STRING)
    @Column(name = "category")
    private TicketCategory ticketCategory;

    @Setter
    @Getter
    private int place;
}
